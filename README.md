# knitknitdraw

A drawing software to make knitting patterns. knitknitdraw is built around the idea of making collections of drawing tools for knitting patterns that can be used with (but not only) the [ayab software](https://github.com/AllYarnsAreBeautiful/ayab-desktop). It tries to make the implementation of a new drawing tool as simple as possible. One can add a new drawing tool to the collection by adding a javascript module in the `Tools` directory. See [Customize / Contribute](#customize-contribute) section for detail.

## Installation
An executable version of knitknitdraw is accessible in the `/dist` folder of this repo. Just open the `/dist/index.html` file directly in your browser.

## Customize / Contribute
In the main directory : `npm install` should install all required dependencies.