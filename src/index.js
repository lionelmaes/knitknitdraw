/**
 * knitknitDraw v0.2
 * Small drawing tool for hacked knitting machine
 *
 * by Lionel Maes - https://lavillahermosa.com/
 *
 * @license GNU GPL v3 or (at your option) any later version.
 * http://www.gnu.org/licenses/gpl.html
 **/

import {sceneInit} from './Scene.js';
import UI from './UI.js';
import View from './View.js';

//DYNAMIC IMPORT OF ALL MODULES IN THE TOOLS DIRECTORY THANKS TO WEBPACK
async function importAll(r) {
  const keys = r
    .keys()
    .filter((key) => key.startsWith('./'))
    .map((key) => key.replace('./', ''));

  const importPromises = keys.map(async (key) => {
    return await import(`./Tools/${key}`);
  });

  try {
    const resolved = await Promise.all(importPromises);
    return resolved;
  } catch (error) {
    console.error(error);
  }
}

const allTools = require.context(
  './Tools',
  false,
  /\.js$/
);

const tools = await importAll(allTools);
//END OF DYNAMIC IMPORT


//sceneInit();

const view = View();

const ui = UI(tools, {
  onToggleGrid: view.toggleGrid,
  onResize: view.resize,
  onZoom: view.zoom,
  onSwitchTool: view.switchTool,
  onHistory:view.changePatternLayer,
  onExport:view.exportPattern,
  onImport:view.importPattern
});