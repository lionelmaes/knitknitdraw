//All EYES ON ME
//create eyes with random gaze directions

import {scene, pixelSize} from '../Scene.js';
import {posToGrid} from './Utils/Drawing.js';
import {eventsHistory, eventsOptions} from './Utils/Events.js';


const allEyes = (layers, $toolOptions) => {
    let color = '#000';
    const pos = {x:null, y:null};

    const update = () => {
        updatePos();
        showBrush();
    };

    const updatePos = () => {
        pos.x = Math.floor(scene.mouseX / pixelSize.get()) * pixelSize.get();
        pos.y = Math.floor(scene.mouseY / pixelSize.get()) * pixelSize.get();
    };

    const showBrush = () => {
        layers.tool.clear();
        layers.tool.noFill();
        layers.tool.stroke(255, 0, 0);
        layers.tool.rect(pos.x, pos.y, 
            options.width.value * pixelSize.get(), 
            options.height.value * pixelSize.get());
    };
    
    const makeEye = () => {
        layers.pattern.noStroke();
        layers.pattern.fill('#000000');
        const pupilDistance = 2;
        const pupilSize = options.pupilsize.value;
        
        const eyeWidth = options.width.value * pixelSize.get() - pixelSize.get();
        const eyeHeight = options.height.value * pixelSize.get() - pixelSize.get();
        const lookAngle = Math.random() * (Math.PI * 2);
        
        const center = {'x' : pos.x + eyeWidth/2, 'y' : pos.y + eyeHeight / 2};
        const pupilPos = posToGrid({
            'x':center.x  - (pupilSize * pixelSize.get()) / 2 + Math.cos(lookAngle) * (pupilDistance * pixelSize.get()),
            'y':center.y   - (pupilSize * pixelSize.get()) / 2 + Math.sin(lookAngle) * (pupilDistance * pixelSize.get())
        });

        
        for(let x = pupilPos.x; x < pupilPos.x + pupilSize * pixelSize.get(); x += pixelSize.get()){
            for(let y = pupilPos.y; y < pupilPos.y + pupilSize * pixelSize.get(); y += pixelSize.get()){
                layers.pattern.rect(x, y, pixelSize.get(), pixelSize.get());
            }
        }
        for(let i = 0; i < options.resolution.value; i++){
            
            const pxPos = posToGrid({
                'x': Math.round(center.x + Math.cos(((Math.PI * 2) / options.resolution.value) * i) * (eyeWidth / 2)) % scene.width,
                'y' : Math.round(center.y + Math.sin(((Math.PI * 2) / options.resolution.value) * i) * (eyeHeight / 2)) % scene.height
            });
            layers.pattern.rect(pxPos.x, pxPos.y, pixelSize.get(), pixelSize.get());
        }
    

    };
    
    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers);
        scene.mousePressed = (e) => {
            if(e.target.tagName != 'CANVAS') 
                return;
            
            makeEye();
        }

    };

    
    initEvents();

    return {update};
}

export const toolName = 'All Eyes On Me';

export const options = {
    'width': {
        'name' : 'width', 
        'type' : 'number', 
        'min': 4, 
        'max' : 60,
        'value' : 10,
        'event' : 'change',
        'action' :'set-option'
    },
    'height': {
        'name' : 'height', 
        'type' : 'number', 
        'min': 4, 
        'max' : 60,
        'value' : 10,
        'event' : 'change',
        'action' :'set-option'
    },
    'resolution':{
        'name' : 'resolution',
        'type' : 'number', 
        'min': 20, 
        'max' : 50,
        'value' : 20,
        'event' : 'change',
        'action' :'set-option'
    },
    'pupilsize':{
        'name' : 'pupil size',
        'type' : 'number', 
        'min': 1, 
        'max' : 5,
        'value' : 3,
        'event' : 'change',
        'action' :'set-option'
    }
};
    

export const tool = allEyes;

