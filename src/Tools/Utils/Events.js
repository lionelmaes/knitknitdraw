import history from '../../History.js';
import {scene} from '../../Scene.js';

const eventsOptions = ($toolOptions, options) => {
    $toolOptions.querySelectorAll('.action').forEach($button => {
        const event = $button.getAttribute('data-action-type');
        const action = $button.getAttribute('data-action');
        const setting = $button.getAttribute('data-setting');
        $button.addEventListener(event, e => {
            switch(action){
                case 'set-option':
                    options[setting].value = $button.value;
                    break;
            }
        });
    });
};

const eventsHistory = (event, layers) => {
    
    scene[event] = (e) => {
        console.log(e.target);
        if(e.target.nodeName == "CANVAS"){
            history.push(layers.pattern);
        }
    };
    
};

export {eventsOptions, eventsHistory};