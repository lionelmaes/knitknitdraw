import { scene, pixelSize } from "../../Scene";

const posToGrid = (pos, mult = 1) => {
    const cellSize = pixelSize.get() * mult;
    //console.log(mult);
    return {
        'x' : Math.round(pos.x / cellSize) * cellSize,
        'y' : Math.round(pos.y / cellSize) * cellSize
    }
};

const copyLayer = (layer) => {
    const newLayer = scene.createGraphics(layer.width, layer.height);
    newLayer.noSmooth();
    newLayer.image(layer, 0, 0, layer.width, layer.height);
    return newLayer;
};

const getPxColor = (x, y, pixels, width) => {
    const d = scene.pixelDensity();
    const i = 4 * d * (y * d * width + x);
    //if it's transparent, we return white
    return (pixels[i+3] == 0)?255:pixels[i];
                
};

export { posToGrid, copyLayer, getPxColor };