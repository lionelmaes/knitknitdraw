//MAGIC FILL
//no more than x consecutive pixels (stitches) of the same color
import { scene, pixelSize } from '../Scene.js';
import { eventsHistory, eventsOptions } from './Utils/Events.js';
import { getPxColor } from './Utils/Drawing.js';

const magicFill = (layers, $toolOptions) => {
   
    const update = () => {
        showBrush();
    };


    const showBrush = () => {
        layers.tool.clear();
        layers.tool.noFill();
        layers.tool.stroke(255, 0, 0);
        layers.tool.rect(scene.mouseX, scene.mouseY, pixelSize.get(), pixelSize.get());
    };
    
    const makeGrid = () => {
        for(let y = 0; y < layers.pattern.height; y += pixelSize.get()){
            for(let x = pixelSize.get() * options.maxcol.value; x < layers.pattern.width; x += pixelSize.get() * options.maxcol.value){
                layers.pattern.fill(0);
                layers.pattern.noStroke();
                layers.pattern.rect(x, y, pixelSize.get(), pixelSize.get());
            }
        }
    };

    const simpleMagic = () => {
        layers.pattern.loadPixels();
        
        
        for(let y = 0; y < layers.pattern.height; y += pixelSize.get()){
            let currentColor = -1;
            let sameColor = 0;
            for(let x = 0; x < layers.pattern.width; x += pixelSize.get()){
            
                const color = getPxColor(x, y, layers.pattern.pixels, layers.pattern.width);
                
                //console.log(color);

                if(color != currentColor){
                    currentColor = color;
                    sameColor = 1;
                }
                else{
                    sameColor++;
                }
                if(sameColor > options.maxcol.value){
                    const newColor = (currentColor == 255)?0:255;
                    layers.pattern.fill(newColor);
                    layers.pattern.noStroke();
                    layers.pattern.rect(x, y, pixelSize.get(), pixelSize.get());
                    currentColor = newColor;
                    sameColor = 1;
                    
                }

              
            }

        }
        
    };
    
    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers);

        scene.mousePressed = (e) => {
            if(e.target.tagName != 'CANVAS') 
                return;
            switch(Number(options.version.value)){
                case 0:
                    simpleMagic();
                    break;
                case 1:
                    //first apply grid then simpleMagic
                    makeGrid();
                    simpleMagic();
                    break;
            }
        };
    };

    
    initEvents();

    return {update};
}

export const toolName = 'Magic Fill';
export const options = {
    'version': {
        'name' : 'version', 
        'type' : 'select', 
        'options': [
            {'name':'version switch', 'value':0, 'default':true},
            {'name':'version grid', 'value':1}
        ],
        'event' : 'change',
        'action' :'set-option',
        'value':0
    },
    'maxcol': {
        'name' : 'identical stitches', 
        'type' : 'number', 
        'min': 2, 
        'max' : 10,
        'value' : 2,
        'event' : 'change',
        'action' :'set-option'
    }
};



export const tool = magicFill;