//Puller
//take one stitch and pull

import {scene, pixelSize} from '../Scene.js';
import {eventsHistory, eventsOptions} from './Utils/Events.js';
import { posToGrid } from './Utils/Drawing.js';

const puller = (layers, $toolOptions) => {
    let pos, oldPos;
    let particles = [];

    const update = () => {
        
        pos = posToGrid({'x':scene.mouseX, 'y':scene.mouseY});

        if(scene.mouseIsPressed && oldPos != undefined 
            && (pos.x != oldPos.x || pos.y != oldPos.y)
            && pos.x > 0 && pos.y > 0){
            computeParticles(oldPos, pos);
            drawParticles();
        }
        oldPos = pos;
        
        showBrush();
    };


    const drawParticles = () => {
        layers.pattern.background(255);
        particles.forEach(particle => {
            let particlePos = posToGrid({'x':particle.x, 'y':particle.y});
            layers.pattern.rect(particlePos.x, particlePos.y, pixelSize.get(), pixelSize.get());

        });
    };

    const computeParticles = (oldPos, pos) => {
        const distance = scene.dist(oldPos.x, oldPos.y, pos.x, pos.y);
        const angle = Math.atan2(pos.y - oldPos.y, pos.x - oldPos.x);
        
        particles.forEach(particle => {
            const distanceFromPos = scene.dist(pos.x, pos.y, particle.x, particle.y);
            particle.x = particle.x + Math.cos(angle) * (distance / distanceFromPos * options.strength.value);
            particle.y = particle.y + Math.sin(angle) * (distance / distanceFromPos * options.strength.value);
            //particles[i] = posToGrid(particle);
        });

    };
   

    const showBrush = () => {
        layers.tool.clear();
        

        layers.tool.line(pos.x, pos.y - 10, pos.x, pos.y + 10);
        layers.tool.line(pos.x - 10, pos.y, pos.x + 10, pos.y);
    };
    
    
    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers, makeParticles);
        scene.mousePressed = () => {
            layers.pattern.noStroke();
            layers.pattern.fill(0);
        };
        
    };

    const makeParticles = () => {
        particles = [];
        for(let x = 0; x < layers.pattern.width; x += pixelSize.get()){
            for(let y = 0; y < layers.pattern.height; y += pixelSize.get()){
                const color = layers.pattern.get(x, y);
                if(color[3] != 0 && color[0] == 0){
                    console.log(layers.pattern.get(x, y));
                    particles.push({'x':x, 'y':y});
                }

            }
        }
    }

    const onPatternLayerChange = () => {
      makeParticles();  
    };

    const onZoom = onPatternLayerChange;

    
    initEvents();
    makeParticles();
    layers.tool.noFill();
    layers.tool.stroke(255, 0, 0);


    return {update, onPatternLayerChange, onZoom};
}

export const toolName = 'Puller';
export const options = {
    'strength': {
        'name' : 'strength', 
        'type' : 'number', 
        'min': 10, 
        'max' : 200,
        'value' : 50,
        'event' : 'change',
        'action' :'set-option'
    }
};
    

export const tool = puller;