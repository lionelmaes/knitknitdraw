//STROKE
//drawing in the grid

import {scene, pixelSize} from '../Scene.js';
import {eventsHistory, eventsOptions} from './Utils/Events.js';
import { posToGrid } from './Utils/Drawing.js';

const stroke = (layers, $toolOptions) => {
    let color = '#000', brushSize;
    let pos = {x:null, y:null};

    const update = () => {
        brushSize = pixelSize.get() * options.size.value;
        pos = posToGrid({'x':scene.mouseX, 'y':scene.mouseY});
        showBrush();
        if(scene.mouseIsPressed){
            layers.pattern.fill(color);
            layers.pattern.noStroke();
            layers.pattern.rect(pos.x, pos.y, brushSize, brushSize);
        }
    };

    const showBrush = () => {
        layers.tool.clear();
        layers.tool.noFill();
        layers.tool.stroke(255, 0, 0);

        layers.tool.rect(pos.x, pos.y, brushSize, brushSize);
    };
    
    
    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers);
    };

    
    initEvents();

    return {update};
}

export const toolName = 'Stroke';
export const options = {
    'size': {
        'name' : 'size', 
        'type' : 'number', 
        'min': 1, 
        'max' : 200,
        'value' : 4,
        'event' : 'change',
        'action' :'set-option'
    }
};
    

export const tool = stroke;