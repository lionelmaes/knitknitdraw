//Patcher
//move selection

import {scene, pixelSize} from '../Scene.js';
import {eventsHistory, eventsOptions} from './Utils/Events.js';
import { posToGrid, copyLayer } from './Utils/Drawing.js';

const patcher = (layers, $toolOptions) => {
    let brushSize;
    let pos, posInit;
    let baseLayer;

    const update = () => {
        brushSize = pixelSize.get() * options.size.value;
        pos = posToGrid({'x':scene.mouseX, 'y':scene.mouseY});
        //console.log(pos);
        showBrush();
    };

    const compute = () => {
        layers.pattern = copyLayer(baseLayer);
        
        layers.pattern.fill(255);
        layers.pattern.noStroke();
        layers.pattern.rect(posInit.x, posInit.y, brushSize, brushSize);
        layers.pattern.copy(baseLayer, posInit.x, posInit.y, brushSize, brushSize,
              pos.x, pos.y, brushSize, brushSize);

        

    };

    const showBrush = () => {
        layers.tool.clear();
        layers.tool.noFill();
        layers.tool.stroke(255, 0, 0);

        layers.tool.rect(pos.x, pos.y, brushSize, brushSize);
    };
    
    
    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers);
        scene.mouseDragged = (e) => {
            if(e.target.nodeName != "CANVAS") return;
            if(posInit == undefined)
                posInit = pos;
            compute();

        }
        scene.mousePressed = (e) => {
            if(e.target.nodeName != "CANVAS") return;
            baseLayer = copyLayer(layers.pattern);
            posInit = pos;
            
        };

    };

    
    initEvents();

    return {update};
}

export const toolName = 'Patcher';
export const options = {
    'size': {
        'name' : 'size', 
        'type' : 'number', 
        'min': 1, 
        'max' : 200,
        'value' : 20,
        'event' : 'change',
        'action' :'set-option'
    }
};
    

export const tool = patcher;