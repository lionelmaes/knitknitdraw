import {scene, pixelSize} from '../Scene.js';
import {eventsOptions, eventsHistory} from './Utils/Events.js';
import { posToGrid } from './Utils/Drawing.js';


const eraser = (layers, $toolOptions) => {
    let brushSize;
    let pos = {x:null, y:null};

    const update = () => {
        brushSize = pixelSize.get() * options.size.value;
        pos = posToGrid({'x':scene.mouseX, 'y':scene.mouseY});
        showBrush();
        if(scene.mouseIsPressed){
            layers.pattern.erase();
            layers.pattern.noStroke();
            layers.pattern.rect(pos.x, pos.y, brushSize, brushSize);
            layers.pattern.noErase();
        }
    };

    const initEvents = () => {
        eventsOptions($toolOptions, options);
        eventsHistory('mouseReleased', layers);
    };

    const showBrush = () => {
        layers.tool.clear();
        layers.tool.noFill();
        layers.tool.stroke(255, 0, 0);

        layers.tool.rect(pos.x, pos.y, brushSize, brushSize);
    };
    
    
    
    initEvents();

    return {update};
}

export const toolName = 'Eraser';
export const options = {
    'size': {
        'name' : 'size', 
        'type' : 'number', 
        'min': 1, 
        'max' : 200,
        'value' : 4,
        'event' : 'change',
        'action' :'set-option'
    }
};
    

export const tool = eraser;