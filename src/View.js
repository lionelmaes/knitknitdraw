
import history from './History.js';
import {scene, pixelSize} from './Scene.js';

const view = () => {


    let $canvas;
    let grid = false;
    let currentTool = null;
    let baseWidth = 100, baseHeight = 100;
    const layers = {'pattern':null, 'tool':null, 'grid':null};

    const init = () => {

        scene.setup = () => {
            $canvas = document.getElementById('render');
            scene.createCanvas(baseWidth * pixelSize.get(), baseHeight * pixelSize.get(), $canvas);
            Object.keys(layers).forEach(key => {
                layers[key] = scene.createGraphics(scene.width, scene.height);
            });
            history.push(layers.pattern);
            buildGrid();
            
        };  
        scene.draw = () => {
            scene.background(255);
            if(currentTool != null){
                scene.noCursor();
                currentTool.update();
            }
            scene.image(layers.pattern, 0, 0);
           
            if(grid){
                scene.image(layers.grid, 0, 0);
            }
            scene.image(layers.tool, 0, 0);
        };
    };

    const buildGrid = () => {
        
        layers.grid = scene.createGraphics(scene.width, scene.height);
        layers.grid.stroke(200);
        for(let x = pixelSize.get(); x <= scene.width - pixelSize.get(); x+=pixelSize.get()){
            layers.grid.line(x, 0, x, layers.grid.height);
        }
        for(let y = pixelSize.get(); y <= scene.height - pixelSize.get(); y+=pixelSize.get()){
            layers.grid.line(0, y, layers.grid.width, y);
        }
        
    }


    const toggleGrid = (value) => {
        //console.log(value);
        grid = (value)?true:false;
   
    };

    const resize = ({width = baseWidth, height = baseHeight} = {}) => {

        scene.resizeCanvas(width * pixelSize.get(), height * pixelSize.get());
        //console.log(layers.grid);
        Object.keys(layers).forEach(key => {
            
            const newLayer = scene.createGraphics(width * pixelSize.get(), height * pixelSize.get());
            newLayer.noSmooth();
            if(key == 'pattern' && layers[key] != null){
                
                newLayer.image(layers[key], 0, 0, width * pixelSize.get(), height * pixelSize.get());
                
            }
            layers[key] = newLayer;
        });
        baseWidth = width;
        baseHeight = height;
        buildGrid();

    };

    const zoom = (value) => {
        pixelSize.set(Math.floor(value / 100 * 4));
        resize();

        if(currentTool != null){
            currentTool.onZoom?.();
        }
        //console.log(pixelSize);
    };


    const switchTool = (tool, $toolOptions) => {
        resetSceneActions();
        currentTool = tool(layers, $toolOptions);
        console.log(currentTool);
    };
    const resetSceneActions = () => {
        scene.mousePressed = null;
        scene.mouseReleased = null;
        scene.mouseDragged = null;

    }
    const changePatternLayer = (patternLayer) => {
        let doResize = false;
        //console.log(patternLayer);
        const layer = scene.createGraphics(patternLayer.width, patternLayer.height);
        layer.image(patternLayer, 0, 0);
        if(layers.pattern.width != patternLayer.width || layers.pattern.height != patternLayer.height){
            doResize = true;
        }
        layers.pattern = layer;
        if(doResize)
            resize({'width':patternLayer.width / pixelSize.get(), 'height':patternLayer.height / pixelSize.get()});

        if(currentTool != null){
            currentTool.onPatternLayerChange?.();
        }
    };

    const exportPattern = () => {
        const exportLayer = scene.createGraphics(baseWidth, baseHeight);
        exportLayer.background(255);
        exportLayer.image(layers.pattern, 0, 0, baseWidth, baseHeight);
        scene.save(exportLayer, 'export', 'png');
        
    };

    const loadFile = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;
    });

    const importPattern = async (file) => {
        const fileData = await loadFile(file);
        const image = scene.loadImage(fileData, () => {
            
            const pattern = scene.createGraphics(image.width * pixelSize.get(), image.height * pixelSize.get());
            pattern.noSmooth();
            pattern.image(image, 0, 0, pattern.width, pattern.height);

            changePatternLayer(pattern);
            history.push(layers.pattern);
            
        });

        


    };


    init();
    return {toggleGrid, resize, zoom, switchTool, changePatternLayer, exportPattern, importPattern}
};

export default view;