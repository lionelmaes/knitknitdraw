import {scene} from './Scene.js';


const History = (() => {
    const memory = [];

    const limit = 20;
    let index = -1;

    const push = (layer) => {
        //we are rewriting history
        if(index < memory.length - 1){
            memory.splice(index + 1, Infinity);
        }
        
        //limit exceeded
        else if(memory.length > limit){
            memory.shift();
        } 
        
        const layerCopy = scene.createGraphics(layer.width, layer.height);
        layerCopy.image(layer, 0, 0);

        memory.push(layerCopy);
        console.log(memory);
        index++;    
    };
    const status = () => {
        return index / memory.length;
    };

    const back = () => {
        if(index > 0){
            index--;
        }
        return memory[index];
    };

    const forward = () => {
        if(index < memory.length - 1){
            index++;
        }
        return memory[index];
    };

    return {push, status, back, forward};

})();


export default History;