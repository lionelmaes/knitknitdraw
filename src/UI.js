import history from './History.js';

const UI = (toolsData, {
    onToggleGrid = null,
    onResize = null,
    onZoom = null,
    onSwitchTool = null,
    onHistory = null,
    onExport = null,
    onImport = null
    } = {}) => {

    const tools = {};
    const toolsOptions = {};

    const $toolsUI = document.querySelector('.tools-ui');
    const $toolSwitch = $toolsUI.querySelector('.tool-switch');
    const $toolOptions = document.querySelector('.tool-options');

    const init = () => {
        initEvents(document);
        buildTools(toolsData);

    };
    
    const buildTools = (toolsData) => {
        
        //console.log(toolsData);
        toolsData.forEach((toolData, i) => {
            const $option = document.createElement('option');
            $option.innerText = toolData.toolName;
            $option.setAttribute('value', i);
            $toolSwitch.append($option);
            tools[i] = toolData.tool;
            toolsOptions[i] = toolData.options;
        });
        

    };
    const resetToolOptions = () => {
        $toolOptions.innerHTML = '';
    };

    const buildToolOptions = (options) => {
        
        Object.keys(options).forEach(alias => {
            const option = options[alias];
            const $option = document.createElement('div');
            
            let optionHTML = `<label for="tool-${alias}">${option.name}</label>`;
            switch(option.type){
                case 'number':
                    optionHTML += `<input id="tool-${alias}" type="number" class="action" 
                    data-action="${option.action}" data-action-type="${option.event}"
                    data-setting="${alias}" min="${option.min}" max="${option.max}" value="${option.value}">`;  
                    break;
                case 'select':
                    optionHTML += `<select id="tool-${alias}" class="action" 
                    data-action="${option.action}" data-action-type="${option.event}"
                    data-setting="${alias}">`;
                    option.options.forEach(selectOption => {
                        optionHTML += `<option value="${selectOption.value}" 
                            ${selectOption.default?'default':''}>${selectOption.name}</option>`;
                    });
                    optionHTML += '</select>';
                    break;        
            }
            $option.innerHTML = optionHTML;
            $toolOptions.append($option);
        });
        return $toolOptions;
    };

    const initEvents = ($parent) => {
        $parent.querySelectorAll('.action').forEach($button => {
            const event = $button.getAttribute('data-action-type');
            const action = $button.getAttribute('data-action');
            $button.addEventListener(event, e => {
                switch(action){
                    case 'set-width':
                        onResize?.({width : $button.value});
                        break;
                    case 'set-height':
                        onResize?.({height : $button.value});
                        break;
                    case 'toggle-grid':
                        onToggleGrid?.($button.checked);
                        break;
                    case 'set-zoom':
                        onZoom?.($button.value);
                        const $valueField = $button.nextElementSibling;
                        
                        if($valueField != null && $valueField.classList.contains('zoom-value')){
                            $valueField.innerText = `${$button.value}%`;
                        }
                        break;
                    case 'undo':
                        onHistory?.(history.back());
                        break;
                    case 'redo':
                        onHistory?.(history.forward());
                        break;
                    case 'switch-tool':
                        resetToolOptions();
                        if($button.value == '') return;
                        const $toolOptions = buildToolOptions(toolsOptions[$button.value]);
                        onSwitchTool?.(tools[$button.value], $toolOptions);
                        break;
                    case 'export':
                        onExport?.();
                        break;
                    case 'trigger':
                        const $target = document.querySelector(`#${$button.value}`);
                        $target.click();
                        break;
                    case 'import':
                        onImport?.($button.files[0]);
                        break;
                    
                    
                }
            });
            if($button.getAttribute('data-init') == 1){
                $button.dispatchEvent(new Event(event));
            }

        });
    };


    init();
};

export default UI;