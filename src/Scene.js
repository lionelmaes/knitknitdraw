
import p5 from 'p5';

let scene = new p5(p => scene = p);

let pixelSize = (() => {
    let size = 4;
    const set = (value) => size = value;
    const get = () => size;
    return {set, get};
})();

export {scene, pixelSize};